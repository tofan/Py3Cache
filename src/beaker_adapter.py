# -*- coding: utf-8 -*-
import configparser

from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options

class BeakerAdapter:

    cache = ''

    def __init__(self):
        config = configparser.ConfigParser()
        config.optionxform = str #保留配置文件key的大小写设定
        config.read("config.ini")
        opts = config.items('beaker')
        self.cache = CacheManager(**parse_cache_config_options(dict(opts)))

    # Read Redis Data
    def get(self, pname, pkey):
        tmpl_cache = self.cache.get_cache(pname)
        return tmpl_cache.get(pkey)

    # Save Redis Data
    def set(self, pname, pkey, pvalue):
        tmpl_cache = self.cache.get_cache(pname)
        tmpl_cache.put(pkey,pvalue)

    # Delete Redis Data
    def evict(self, pname, pkeys):
        tmpl_cache = self.cache.get_cache(pname)
        tmpl_cache.remove_value(key=pkeys)

    def clear(self, pname):
        tmpl_cache = self.cache.get_cache(pname)
        tmpl_cache.clear()

    # Close Redis Connection
    def close(self):
        pass


if __name__ == '__main__':
    ba = BeakerAdapter()
    print(ba.set("users","ld", {'id':12323,'account':'ld','name':'Winter Lau'}))
    print(ba.get("users","ld"))
